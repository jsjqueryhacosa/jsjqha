$(function() {
    $(".graph").graph();
});

$.fn.graph = function () {
    return this.each(function() {
        var $obj = $(this),
            $rlg = $obj.find(".realgraph"),
            
            num = $obj.find("span").text();

        $rlg.animate({
           width: num+"%" 
        });
    });
};
